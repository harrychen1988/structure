package com.harry.base;

import com.harry.base.structure.sort.InsertSort;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by chenyehui on 16/2/15.
 */
public class InsertSortTest extends TestCase {

    protected void setUp() {
    }

    private Integer[] example = {5, 4, 3, 2, 7, 9, 12, 10};

    private Integer[] result = {2, 3, 4, 5, 7, 9, 10, 12};

    @Test
    public void testInsertSort() {
        InsertSort sort = new InsertSort();
        sort.sort(example);
        assertTrue(Arrays.equals(example, result));
    }

}
