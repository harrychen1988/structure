package com.harry.base.structure;

import com.harry.base.structure.sort.FastSort;
import com.harry.base.structure.sort.InsertSort;
import com.harry.base.structure.sort.MergeSort;
import com.harry.base.structure.sort.PuperSort;

/**
 * Created by chenyehui on 16/2/15.
 */
public class SortShoot {

    private static Integer[] example = {3, 4, 5, 2, 9, 8, 6};

    public static void main(String[] args) {
        Sort sort = new MergeSort();
        sort.sort(example);

        printArray();
    }

    public static void printArray(){
        for(Integer i : example){
            System.out.print(i + " ");
        }
    }

    public static void printArray(Integer[] example){
        for(Integer i : example){
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
