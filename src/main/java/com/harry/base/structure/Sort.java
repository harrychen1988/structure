package com.harry.base.structure;

/**
 * Created by chenyehui on 16/2/15.
 */
public interface Sort {

    void sort(Integer[] source);
}
