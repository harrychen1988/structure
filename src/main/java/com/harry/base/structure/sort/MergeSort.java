package com.harry.base.structure.sort;

import com.harry.base.structure.Sort;

/**
 * Created by chenyehui on 16/2/16.
 */
public class MergeSort implements Sort {

    @Override
    public void sort(Integer[] source) {
        sort(source, 0, source.length - 1);
    }

    public void sort(Integer[] source, int left, int right) {
        if (left >= right) {
            return;
        }
        Integer center = (left + right) / 2;
        sort(source, left, center);
        sort(source, center + 1, right);
        merge(source, left, center, right);
    }

    private void merge(Integer[] source, Integer begin, Integer mid, Integer end) {
        Integer[] temp = new Integer[source.length];
        Integer center = mid + 1;
        Integer b = begin;
        Integer a = begin;

        while (begin <= mid && center <= end) {
            if (source[begin] < source[center]) {
                temp[b++] = source[begin++];
            } else {
                temp[b++] = source[center++];
            }
        }

        while (begin <= mid) {
            temp[b++] = source[begin++];
        }

        while (center <= end) {
            temp[b++] = source[center++];
        }

        while (a <= end) {
            source[a] = temp[a++];
        }
    }
}
