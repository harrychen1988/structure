package com.harry.base.structure.sort;

import com.harry.base.structure.Sort;

/**
 * Created by chenyehui on 16/2/15.
 */
public class PuperSort implements Sort {

    @Override
    public void sort(Integer[] source) {
        for (int i = 0; i < source.length; i++) {
            int min = source[i];
            int mark = i;
            for (int j = i; j < source.length; j++) {
                if (source[j] < min) {
                    min = source[j];
                    mark = j;
                }
            }
            Integer temp = source[mark];
            source[mark] = source[i];
            source[i] = temp;
        }
    }
}
