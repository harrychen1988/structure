package com.harry.base.structure.sort;

import com.harry.base.structure.Sort;

/**
 * Created by chenyehui on 16/2/15.
 * <p/>
 * 2 3 4 6 5 9 8 7
 */
public class FastSort implements Sort {

    @Override
    public void sort(Integer[] source) {
        sort(source, 0, source.length - 1);
    }

    public void sort(Integer[] source, Integer begin, Integer end) {
        if (begin < end) {
            int i = begin, j = end;
            Integer t = source[begin];
            while (begin < end) {
                while (begin < end && source[begin] < t) {
                    begin++;
                }

                while (begin < end && source[end] > t) {
                    end--;
                }
                // change
                Integer temp = source[begin];
                source[begin] = source[end];
                source[end] = temp;
            }

            sort(source, i, begin - 1);
            sort(source, begin + 1, j);
        }
    }
}
