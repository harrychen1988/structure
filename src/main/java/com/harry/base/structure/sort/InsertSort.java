package com.harry.base.structure.sort;

import com.harry.base.structure.Sort;

/**
 * Created by chenyehui on 16/2/15.
 * <p/>
 * 2  3  1  6  8
 * <p/>
 * <-  *
 */
public class InsertSort implements Sort {

    @Override
    public void sort(Integer[] source) {
        Integer temp;
        for (int i = 1; i < source.length; i++) {
            int j = i;
            temp = source[i];
            while (j > 0 && temp < source[j - 1]) {
                source[j] = source[j - 1];
                j--;
            }
            source[j] = temp;
        }
    }
}
